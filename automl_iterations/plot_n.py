'''
Created on Nov 9, 2015

@author: manju
'''

import os
import glob
import re
import numpy

import matplotlib
import matplotlib.pyplot as plt

def main():
    
    num_regex = "[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?"
    regex = re.compile("^### Score \((?P<metric>.+)\): (?P<score>%s) \((?P<set>.+), iter=(?P<n>\d+)\)$" %(num_regex))
    
    logs = glob.glob("logs/*.log")
    
    for l in logs:
        
        _, basename = os.path.split(l)
        
        x = []
        y = []
        with open(l) as fp:
            for line in fp:
                if line.startswith("###"):
                    line = line.replace("\n","")
                    # ### Score (f1_metric): 0.2064 (albert, iter=1)
                    m = regex.match(line)
                    metric = m.group("metric")
                    score = m.group("score")
                    set_ = m.group("set")
                    n = m.group("n")
                    x.append(n)
                    y.append(score)
                    print(set_)
        
        basename = basename.replace(".log","")            
        fig2 = plt.figure()
        ax2 = fig2.add_subplot(111)
        ax2.plot(x,y)
        plt.title(basename)
        plt.xlabel("n")
        plt.ylabel(metric)
        #plt.show()
        plt.xscale("log")
        
        plt.savefig(basename+".pdf", format="pdf")


if __name__ == '__main__':
    main()