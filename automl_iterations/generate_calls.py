
base = "ve/bin/python run.py data/ out/ %s %s 1> %s_%s.log 2>&1"
models = ["RandomForestClassifier", "GradientBoostingClassifier", "SGDClassifier"] # PassiveAggressiveClassifier"]
data = ["albert", "dilbert", "fabert", "robert", "volkert"]

for m in models:
    for d in data:
        print(base %(m, d, m, d))